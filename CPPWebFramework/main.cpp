#include <QCoreApplication>
#include <cwf/cppwebapplication.h>

int main(int argc, char *argv[])
{
    CWF::CppWebApplication a(argc, argv, CWF::Configuration("/home/herik/cppweb/prototipos/CPPWebFramework/server/"));

    return a.start();
}
