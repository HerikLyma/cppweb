#include "mainwindow.h"
#include <QApplication>
#include <entidade/cliente.h>

int main(int argc, char *argv[])
{
    qRegisterMetaType<Cliente>("Cliente");

    QApplication a(argc, argv);
    MainWindow w;
    w.show();

    return a.exec();
}
