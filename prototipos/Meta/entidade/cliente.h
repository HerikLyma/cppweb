#ifndef CLIENTE_H
#define CLIENTE_H

#include <QObject>

class Cliente : public QObject
{
    Q_OBJECT
    Q_PROPERTY(QString nome READ getNome WRITE setNome);
    Q_PROPERTY(int     id   READ getId   WRITE setId);
private:
    int     id;
    QString nome;
public:
    explicit Cliente(QObject *parent = 0);            
    explicit Cliente(const Cliente &cliente, QObject *parent = 0);
public slots:
    int getId() const;

    void setId(int value);

    QString getNome() const;

    void setNome(const QString &value);
};



#endif // CLIENTE_H
