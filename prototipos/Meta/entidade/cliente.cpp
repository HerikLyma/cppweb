#include "cliente.h"


Cliente::Cliente(QObject *parent) : QObject(parent)
{
    setObjectName("Cliente");
}

Cliente::Cliente(const Cliente &cliente, QObject *parent) : Cliente(parent)
{

}

int Cliente::getId() const
{
    return id;
}

void Cliente::setId(int value)
{
    id = value;
}

QString Cliente::getNome() const
{
    return nome;
}

void Cliente::setNome(const QString &value)
{
    nome = value;
}
