#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <meta/metaclassparser.h>
#include <entidade/cliente.h>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    ui->lineEdit->setReadOnly(true);
    ui->plainTextEdit->setReadOnly(true);
    ui->plainTextEdit_2->setReadOnly(true);

    Cliente *c = (Cliente*)MetaClassParser::instantiateClassByName("Cliente");
    if(c)
    {
        MetaClassParser metaParser(c);
        ui->lineEdit->setText(c->objectName());

        for(auto it = metaParser.methods.begin(); it != metaParser.methods.end(); ++it)
            ui->plainTextEdit->setPlainText(ui->plainTextEdit->toPlainText() + std::get<0>(it.key()) + " " + std::get<1>(it.key()) + "\n");

        for(auto it = metaParser.properties.begin(); it != metaParser.properties.end(); ++it)
            ui->plainTextEdit_2->setPlainText(ui->plainTextEdit_2->toPlainText() + it.key() + "\n");

        ui->plainTextEdit->setPlainText(ui->plainTextEdit->toPlainText() + "\n\n\n\n");
        ui->plainTextEdit->setPlainText(ui->plainTextEdit->toPlainText() + metaParser.getReturnType("getId()"));
    }    
}

MainWindow::~MainWindow()
{
    delete ui;
}
