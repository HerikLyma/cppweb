#-------------------------------------------------
#
# Project created by QtCreator 2015-08-12T10:58:20
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = Meta
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    entidade/cliente.cpp \
    meta/metaclassparser.cpp

HEADERS  += mainwindow.h \
    entidade/cliente.h \
    meta/metaclassparser.h

FORMS    += mainwindow.ui

QMAKE_CXXFLAGS += -std=c++11
