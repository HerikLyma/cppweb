#ifndef SESSIONVALIDATORFILTER_H
#define SESSIONVALIDATORFILTER_H

#include <cwf/filter.h>
#include <cwf/httpservletrequest.h>
#include <cwf/httpservletresponse.h>

class SessionValidatorFilter : public CWF::Filter
{
public:
    virtual ~SessionValidatorFilter(){}
    void doFilter(CWF::HttpServletRequest &req,
                  CWF::HttpServletResponse &resp,
                  CWF::FilterChain &chain)
    {
        chain.doFilter(req, resp);
        return;
        QString url = req.getRequestURL();
        if(url.endsWith(".css") ||
           url.endsWith(".png") ||
           url.endsWith(".jpg"))
        {
            chain.doFilter(req, resp);
        }
        else if(url != "/login")
        {
            QObject *obj = req.getSession().
                           getAttribute("user");
            if(obj == nullptr || req.getSession().isExpired())
            {
                req.getRequestDispatcher("/pages/login").
                                      forward(req, resp);
            }
            else
            {
                chain.doFilter(req, resp);
            }
        }
        else
        {
            chain.doFilter(req, resp);
        }
    }
};

#endif // SESSIONVALIDATORFILTER_H
