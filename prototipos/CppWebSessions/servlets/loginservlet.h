#ifndef LOGINSERVLET_H
#define LOGINSERVLET_H

#include <cwf/httpservlet.h>
#include <cwf/httpservletrequest.h>
#include <cwf/httpservletresponse.h>
#include <classes/user.h>
#include <servlets/mainpageservlet.h>

class LoginServlet : public CWF::HttpServlet
{
public:
    virtual ~LoginServlet(){}
    void doGet(CWF::HttpServletRequest &req,
               CWF::HttpServletResponse &resp)
    {
        req.getRequestDispatcher("/pages/login").
                              forward(req, resp);
    }
    void doPost(CWF::HttpServletRequest &req,
                CWF::HttpServletResponse &resp)
    {
        QString login = req.getParameter("login");
        QString password = req.
                         getParameter("password");
        if(login == "Herik" && password == "1234")
        {
            User *user = new User;
            user->setLogin(login);
            user->setPassword(password);
            req.getSession().addAttribute("user", user);

            MainPageServlet mainPageServlet;
            mainPageServlet.doGet(req, resp);
        }
        else
        {
            req.getRequestDispatcher("/pages/login").
                                  forward(req, resp);
        }
    }
};

#endif // LOGINSERVLET_H

