#ifndef FORSERVLET_H
#define FORSERVLET_H

#include <cwf/httpservlet.h>
#include <cwf/httpservletrequest.h>
#include <cwf/httpservletresponse.h>

class ForServlet : public CWF::HttpServlet
{
public:
    virtual ~ForServlet(){}
    void doGet(CWF::HttpServletRequest &req,
               CWF::HttpServletResponse &resp)
    {
        req.getRequestDispatcher("/pages/for").
                              forward(req, resp);
    }
};

#endif // FORSERVLET_H


