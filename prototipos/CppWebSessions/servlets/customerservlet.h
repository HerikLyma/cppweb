#ifndef CUSTOMERSERVLET_H
#define CUSTOMERSERVLET_H

#include <cwf/httpservlet.h>
#include <cwf/httpservletrequest.h>
#include <cwf/httpservletresponse.h>
#include <cwf/qlistobject.h>
#include <classes/customer.h>

class CustomerServlet : public CWF::HttpServlet
{
public:
    virtual ~CustomerServlet(){}
    void doGet(CWF::HttpServletRequest &req,
               CWF::HttpServletResponse &resp)
    {
        /*
        Customer *customerOne = new Customer;
        Customer *customerTwo = new Customer;
        customerOne->setId(1);
        customerOne->setName("Herik Lima");
        customerOne->setGender('M');
        customerOne->setAddress("555 5th Ave, NY, EUA");

        customerTwo->setId(2);
        customerTwo->setName("Marcelo Eler");
        customerTwo->setGender('M');
        customerTwo->setAddress("500 5th Ave, NY, EUA");

        CWF::QListObject qListObject;
        qListObject.setAutoDelete(true);
        qListObject.add(customerOne);
        qListObject.add(customerTwo);

        req.addAttribute("customersList", &qListObject);
        */
        req.getRequestDispatcher("/pages/customer").
                                 forward(req, resp);

    }

    void doPost(CWF::HttpServletRequest &req,
                CWF::HttpServletResponse &resp)
    {
        Customer *customer = new Customer;              
        req.fillQObject(customer);
        int x;
        int y = x + 10;
        //...
    }
};

#endif // CUSTOMERSERVLET_H



/*
        Customer customer;
        customer.setId(1);
        customer.setName("Herik Lima");
        customer.setGender('M');
        customer.setAddress("555 5th Ave, NY, EUA");

        req.addAttribute("customer", &customer);
        req.getRequestDispatcher("/pages/customer").
                                 forward(req, resp);
*/
