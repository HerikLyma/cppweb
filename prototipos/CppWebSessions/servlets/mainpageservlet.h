#ifndef MAINPAGESERVLET_H
#define MAINPAGESERVLET_H

#include <cwf/httpservlet.h>
#include <cwf/httpservletrequest.h>
#include <cwf/httpservletresponse.h>
#include <classes/user.h>

class MainPageServlet : public CWF::HttpServlet
{
public:
    virtual ~MainPageServlet(){}
    void doGet(CWF::HttpServletRequest &req,
               CWF::HttpServletResponse &resp)
    {
        QObject *user = req.getSession().
                            getAttribute("user");
        if(user && !req.getSession().isExpired())
        {
            req.addAttribute("user", user);
            req.getRequestDispatcher("/pages/mainpage").
                                     forward(req, resp);
        }
        else
        {
            req.getRequestDispatcher("/pages/login").
                                     forward(req, resp);
        }
    }
};

#endif // MAINPAGESERVLET_H



