#include <cwf/cppwebapplication.h>
#include <servlets/loginservlet.h>
#include <servlets/mainpageservlet.h>
#include <filter/sessionvalidatorfilter.h>
#include <servlets/customerservlet.h>
#include <servlets/forservlet.h>

int main(int argc, char *argv[])
{
    CWF::CppWebApplication server(argc,
                                  argv,
                                  CWF::Configuration("/home/herik/cppweb/prototipos/CppWebSessions/server/"),
                                  new SessionValidatorFilter);


    server.addUrlServlet("/login", new LoginServlet);
    server.addUrlServlet("/for", new ForServlet);
    server.addUrlServlet("/mainpage", new MainPageServlet);
    server.addUrlServlet("/customer", new CustomerServlet);

    return server.start();
}
