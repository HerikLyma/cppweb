#ifndef USER_H
#define USER_H

#include <QObject>

class User : public QObject
{
    Q_OBJECT
private:
    int id;
    QString login;
    QString password;
public slots:
    int getId() const
    {
        return id;
    }
    void setId(int value)
    {
        id = value;
    }
    QString getLogin() const
    {
        return login;
    }
    void setLogin(const QString &value)
    {
        login = value;
    }
    QString getPassword() const
    {
        return password;
    }
    void setPassword(const QString &value)
    {
        password = value;
    }

    std::string getChar() const
    {
        return "xxxxxxxxxx";
    }
};

#endif // USER_H
