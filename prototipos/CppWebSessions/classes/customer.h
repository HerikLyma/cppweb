#ifndef CUSTOMER_H
#define CUSTOMER_H

#include <QObject>

class Customer : public QObject
{
    Q_OBJECT
private:
    int id;
    char gender;
    QString name;
    QString address;
public:
    explicit Customer(QObject *o = nullptr) :
                                   QObject(o)
    {}
public slots:
    int getId() const
    {
        return id;
    }
    void setId(int value)
    {
        id = value;
    }
    char getGender() const
    {
        return gender;
    }
    void setGender(char value)
    {
        gender = value;
    }
    QString getName() const
    {
        return name;
    }
    void setName(const QString &value)
    {
        name = value;
    }
    QString getAddress() const
    {
        return address;
    }
    void setAddress(const QString &value)
    {
        address = value;
    }
};

#endif // CUSTOMER_H
