#ifndef CONEXAO_H
#define CONEXAO_H

#include <QtSql/QSqlDatabase>
#include <QSqlQuery>
#include <QSqlRecord>
#include <QVariant>
#include <QSqlError>

class Conexao
{
    QSqlDatabase db;
    QString      connectionName;
public:
    Conexao(const QString &name);
    ~Conexao();
    QSqlDatabase &getConnection();
};

#endif // CONEXAO_H
