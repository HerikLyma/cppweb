#include "conexao.h"


Conexao::Conexao(const QString &name)
{
    connectionName = name + QString::number(rand() % 1000000) + QString::number(rand() % 1000000);
    db = QSqlDatabase::addDatabase("QPSQL", connectionName);
    db.setHostName("localhost");
    db.setPort(5432);
    db.setDatabaseName("GOF");
    db.setUserName("postgres");
    db.setPassword("50ba");
    db.open();
}

Conexao::~Conexao()
{
    db = QSqlDatabase();
    QSqlDatabase::removeDatabase(connectionName);
}

QSqlDatabase &Conexao::getConnection()
{
    return db;
}
