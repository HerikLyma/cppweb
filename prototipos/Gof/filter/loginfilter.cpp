#include "loginfilter.h"

void LoginFilter::doFilter(CWF::HttpServletRequest &request, CWF::HttpServletResponse &response, CWF::FilterChain &chain)
{
    QString url = request.getRequestURL();
    if(url.endsWith(".css") || url.endsWith(".png") || url.endsWith(".jpg"))
    {
        chain.doFilter(request, response);
    }
    else if(url != "/login" && url != "/logar")
    {
        QObject *obj = request.getSession().getAttribute("usuario");
        if(obj == nullptr)
        {            
            request.getRequestDispatcher("/pages/login").forward(request, response);
        }
        else
        {
            if(request.getSession().isExpired())
                request.getRequestDispatcher("/pages/expired").forward(request, response);
            else
                chain.doFilter(request, response);
        }
    }
    else
    {
        chain.doFilter(request, response);
    }
}
