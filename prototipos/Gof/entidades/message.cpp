#include "message.h"

Message::Message(QObject *parent) : QObject(parent)
{

}


QString Message::getMessage()
{
    return m_message;
}

void Message::setMessage(QString message)
{
    m_message = message;
}

bool Message::getStatus()
{
    return m_status;
}

void Message::setStatus(bool status)
{
    m_status = status;
}

