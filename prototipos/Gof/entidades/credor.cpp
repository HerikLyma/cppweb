#include "credor.h"

Credor::Credor(QObject *parent) : QObject(parent)
{
    id = "0";
}

QString Credor::getId()
{
    return id;
}

QString Credor::getNome()
{
    return nome;
}

QString Credor::getUsuId()
{
    return usuId;
}

void Credor::setId(QString id)
{
    this->id = id;
}

void Credor::setNome(QString nome)
{
    this->nome = nome;
}

void Credor::setUsuId(QString usuId)
{
    this->usuId = usuId;
}
