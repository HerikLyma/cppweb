#include "usuario.h"


Usuario::Usuario(QObject *parent) : QObject(parent)
{
    id = "0";
}

QString Usuario::getId()
{
    return id;
}

QString Usuario::getLogin()
{
    return login;
}

QString Usuario::getSenha()
{
    return senha;
}

void Usuario::setId(QString id)
{
    this->id = id;
}

void Usuario::setLogin(QString login)
{
    this->login = login;
}

void Usuario::setSenha(QString senha)
{
    this->senha = senha;
}
