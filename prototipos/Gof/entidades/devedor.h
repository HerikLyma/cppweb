#ifndef DEVEDOR_H
#define DEVEDOR_H

#include <QObject>

class Devedor : public QObject
{
    Q_OBJECT
private:
    QString id;
    QString nome;
    QString usuId;
public:
    explicit Devedor(QObject *parent = 0);
public slots:
    QString getId();
    QString getNome();
    QString getUsuId();

    void setId(QString id);
    void setNome(QString nome);
    void setUsuId(QString usuId);
};

#endif // DEVEDOR_H
