#ifndef MESSAGE_H
#define MESSAGE_H

#include <QObject>

class Message : public QObject
{
    Q_OBJECT
    bool    m_status;
    QString m_message;
public:
    explicit Message(QObject *parent = 0);   
public slots:
    bool getStatus();
    void    setStatus(bool status);

    QString getMessage();
    void    setMessage(QString message);
};

#endif // MESSAGE_H
