#include "devedor.h"

Devedor::Devedor(QObject *parent) : QObject(parent)
{
    id = "0";
}

QString Devedor::getId()
{
    return id;
}

QString Devedor::getNome()
{
    return nome;
}

QString Devedor::getUsuId()
{
    return usuId;
}

void Devedor::setId(QString id)
{
    this->id = id;
}

void Devedor::setNome(QString nome)
{
    this->nome = nome;
}

void Devedor::setUsuId(QString usuId)
{
    this->usuId = usuId;
}
