#ifndef CREDOR_H
#define CREDOR_H

#include <QObject>

class Credor : public QObject
{
    Q_OBJECT
private:
    QString id;
    QString nome;
    QString usuId;
public:
    explicit Credor(QObject *parent = 0);
public slots:
    QString getId();
    QString getNome();
    QString getUsuId();

    void setId(QString id);
    void setNome(QString nome);
    void setUsuId(QString usuId);
};

#endif // CREDOR_H
