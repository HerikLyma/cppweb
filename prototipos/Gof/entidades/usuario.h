#ifndef USUARIO_H
#define USUARIO_H

#include <QString>
#include <QObject>

class Usuario : public QObject
{
    Q_OBJECT
private:
    QString id;
    QString login;
    QString senha;
public:
    explicit Usuario(QObject *parent = 0);
public slots:
    QString getId();
    QString getLogin();
    QString getSenha();
    void    setId(QString id);
    void    setLogin(QString login);
    void    setSenha(QString senha);
};

#endif // USUARIO_H
