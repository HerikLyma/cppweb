#include <QCoreApplication>
#include "cwf/cppwebapplication.h"
#include <servlets/cadastrarcredorservlet.h>
#include <servlets/cadastrardevedorservlet.h>
#include <servlets/credoresservlet.h>
#include <servlets/deletarcredorservlet.h>
#include <servlets/deletardevedorservlet.h>
#include <servlets/devedoresservlet.h>
#include <servlets/entradasservlet.h>
#include <servlets/homeservlet.h>
#include <servlets/logarservlet.h>
#include <servlets/loginservlet.h>
#include <servlets/saidasservlet.h>
#include <servlets/salvarcredorservlet.h>
#include <servlets/salvardevedorservlet.h>
#include <filter/loginfilter.h>

int main(int argc, char *argv[])
{        
    CWF::CppWebApplication server(argc, argv, CWF::Configuration("/home/herik/cppweb/prototipos/Gof/server/"), new LoginFilter);

    server.addUrlServlet("/login",             new LoginServlet);
    server.addUrlServlet("/logar",             new LogarServlet);
    server.addUrlServlet("/home",              new HomeServlet);
    server.addUrlServlet("/devedores",         new DevedoresServlet);
    server.addUrlServlet("/credores",          new CredoresServlet);
    server.addUrlServlet("/saidas",            new SaidasServlet);
    server.addUrlServlet("/entradas",          new EntradasServlet);
    server.addUrlServlet("/cadastrarcredores", new CadastrarCredorServlet);
    server.addUrlServlet("/deletarcredores",   new DeletarCredorServlet);
    server.addUrlServlet("/salvarcredor",      new SalvarCredorServlet);
    server.addUrlServlet("/deletardevedores",  new DeletarDevedorServlet);
    server.addUrlServlet("/cadastrardevedores",new CadastrarDevedorServlet);
    server.addUrlServlet("/salvardevedor",     new SalvarDevedorServlet);    

    return server.start();
}
