#ifndef CREDORDAO_H
#define CREDORDAO_H

#include "cwf/qlistobject.h"
#include <entidades/credor.h>
#include <entidades/message.h>
#include <db/conexao.h>
#include <entidades/credor.h>

class CredorDao : public QObject
{    
    Q_OBJECT
private:
    QString connectionName;
public:
    explicit CredorDao(QString connectionName, QObject *parent = 0);

    CWF::QListObject *getAll(QString usuId);

    Credor *getById(QString id);        

    Message *deletar(int id);

    Message *inserir(Credor &cre);

    Message *atualizar(Credor &cre);
};

#endif // CREDORDAO_H
