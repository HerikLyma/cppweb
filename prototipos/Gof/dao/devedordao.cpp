#include "devedordao.h"
#include <db/conexao.h>
#include <entidades/devedor.h>


DevedorDao::DevedorDao(QString connectionName, QObject *parent) : QObject(parent)
{
    this->connectionName = connectionName;
}

CWF::QListObject *DevedorDao::getAll(QString usuId)
{
    CWF::QListObject *list = new CWF::QListObject;
    Conexao con(connectionName);
    QSqlQuery devedor(con.getConnection());
    devedor.prepare("select * from Devedor where usu_id = :id");
    devedor.bindValue(":id", usuId);
    if(devedor.exec())
    {
        devedor.first();
        while(devedor.isValid())
        {
            Devedor *c = new Devedor;
            c->setId(devedor.record().value("dev_id").toString());
            c->setNome(devedor.record().value("dev_nome").toString());
            c->setUsuId(devedor.record().value("usu_id").toString());

            list->add(c);

            devedor.next();
        }
    }

    return list;
}

Devedor *DevedorDao::getById(QString id)
{
    Conexao      con(connectionName);
    QSqlQuery devedor(con.getConnection());
    devedor.prepare("select * from Devedor where dev_id = :id");
    devedor.bindValue(":id", id);
    Devedor *c = new Devedor;
    if(devedor.exec())
    {
        devedor.first();
        c->setId(devedor.record().value("dev_id").toString());
        c->setNome(devedor.record().value("dev_nome").toString());
        c->setUsuId(devedor.record().value("usu_id").toString());
    }
    return c;
}

Message *DevedorDao::deletar(int id)
{
    Conexao      con(connectionName);
    QSqlQuery devedor(con.getConnection());
    devedor.prepare("delete from Devedor where dev_id = :id");
    devedor.bindValue(":id", id);

    Message *m = new Message;
    if(devedor.exec())
    {
        m->setStatus(true);
        m->setMessage("Deletado com sucesso.");
    }
    else
    {
        m->setStatus(false);
        m->setMessage("Ocorreu um erro.<br><br>" + devedor.lastError().text());
    }
    return m;
}

Message *DevedorDao::inserir(Devedor &dev)
{
    Conexao      con(connectionName);
    QSqlQuery devedor(con.getConnection());
    devedor.prepare("insert into Devedor (usu_id, dev_nome) values(:id, :nome);");
    devedor.bindValue(":id", dev.getUsuId().toInt());
    devedor.bindValue(":nome", dev.getNome());

    Message *m = new Message;
    if(devedor.exec())
    {
        m->setStatus(true);
        m->setMessage("Inserido com sucesso.");
    }
    else
    {
        m->setStatus(false);
        m->setMessage("Ocorreu um erro.<br><br>" + devedor.lastError().text());
    }
    return m;
}

Message *DevedorDao::atualizar(Devedor &dev)
{
    Conexao      con(connectionName);
    QSqlQuery devedor(con.getConnection());
    devedor.prepare("update Devedor set dev_nome = :nome where dev_id = :id;");
    devedor.bindValue(":id",   dev.getId().toInt());
    devedor.bindValue(":nome", dev.getNome());

    Message *m = new Message;
    if(devedor.exec())
    {
        m->setStatus(true);
        m->setMessage("Alterado com sucesso.");
    }
    else
    {
        m->setStatus(false);
        m->setMessage("Ocorreu um erro.<br><br>" + devedor.lastError().text());
    }
    return m;
}
