#ifndef DEVEDORDAO_H
#define DEVEDORDAO_H

#include "cwf/qlistobject.h"
#include <entidades/devedor.h>
#include <entidades/message.h>

class DevedorDao : public QObject
{    
    Q_OBJECT
private:
    QString connectionName;
public:
    explicit DevedorDao(QString connectionName, QObject *parent = 0);

    CWF::QListObject *getAll(QString usuId);

    Devedor *getById(QString id);        

    Message *deletar(int id);

    Message *inserir(Devedor &dev);

    Message *atualizar(Devedor &dev);
};

#endif // DEVEDORDAO_H
