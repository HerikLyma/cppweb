#include "usuariodao.h"


UsuarioDao::UsuarioDao(QString connectionName, QObject *parent) : QObject(parent)
{
    this->connectionName = connectionName;
}

UsuarioDao::~UsuarioDao()
{
    QSqlDatabase::removeDatabase(connectionName);
}

Usuario *UsuarioDao::logar(QString login, QString senha)
{
    Usuario *usu = nullptr;

    Conexao con(connectionName);
    QSqlQuery usuario(con.getConnection());

    usuario.prepare("select * from usuario where upper(usu_login) = upper(:login) and usu_senha = :senha");
    usuario.bindValue(":login", login);
    usuario.bindValue(":senha", senha);
    if(usuario.exec())
    {
        usuario.first();
        if(usuario.isValid())
        {
            usu = new Usuario;
            usu->setId(usuario.record().value("usu_id").toString());
            usu->setLogin(usuario.record().value("usu_login").toString());
            usu->setSenha(usuario.record().value("usu_senha").toString());
        }
    }

    return usu;
}
