#ifndef USUARIODAO_H
#define USUARIODAO_H

#include <db/conexao.h>
#include <entidades/usuario.h>

class UsuarioDao : public QObject
{
    Q_OBJECT
private:
    QString connectionName;
public:
    explicit UsuarioDao(QString connectionName, QObject *parent = 0);

    ~UsuarioDao();

    Usuario *logar(QString login, QString senha);
};

#endif // USUARIODAO_H
