#include "credordao.h"


CredorDao::CredorDao(QString connectionName, QObject *parent) : QObject(parent)
{
    this->connectionName = connectionName;
}

CWF::QListObject *CredorDao::getAll(QString usuId)
{
    CWF::QListObject *list = new CWF::QListObject;
    Conexao con(connectionName);
    QSqlQuery credor(con.getConnection());
    credor.prepare("select * from credor where usu_id = :id");
    credor.bindValue(":id", usuId);
    if(credor.exec())
    {
        credor.first();
        while(credor.isValid())
        {
            Credor *c = new Credor;
            c->setId(credor.record().value("cre_id").toString());
            c->setNome(credor.record().value("cre_nome").toString());
            c->setUsuId(credor.record().value("usu_id").toString());

            list->add(c);

            credor.next();
        }
    }

    return list;
}

Credor *CredorDao::getById(QString id)
{
    Conexao con(connectionName);
    QSqlQuery credor(con.getConnection());
    credor.prepare("select * from credor where cre_id = :id");
    credor.bindValue(":id", id);
    Credor *c = new Credor;
    if(credor.exec())
    {
        credor.first();
        c->setId(credor.record().value("cre_id").toString());
        c->setNome(credor.record().value("cre_nome").toString());
        c->setUsuId(credor.record().value("usu_id").toString());
    }
    return c;
}

Message *CredorDao::deletar(int id)
{
    Conexao con(connectionName);
    QSqlQuery credor(con.getConnection());
    credor.prepare("delete from credor where cre_id = :id");
    credor.bindValue(":id", id);

    Message *m = new Message;
    if(credor.exec())
    {
        m->setStatus(true);
        m->setMessage("Deletado com sucesso.");
    }
    else
    {
        m->setStatus(false);
        m->setMessage("Ocorreu um erro.<br><br>" + credor.lastError().text());
    }
    return m;
}

Message *CredorDao::inserir(Credor &cre)
{
    Conexao      con(connectionName);
    QSqlQuery credor(con.getConnection());
    credor.prepare("insert into credor (usu_id, cre_nome) values(:id, :nome);");
    credor.bindValue(":id", cre.getUsuId().toInt());
    credor.bindValue(":nome", cre.getNome());

    Message *m = new Message;
    if(credor.exec())
    {
        m->setStatus(true);
        m->setMessage("Inserido com sucesso.");
    }
    else
    {
        m->setStatus(false);
        m->setMessage("Ocorreu um erro.<br><br>" + credor.lastError().text());
    }
    return m;
}

Message *CredorDao::atualizar(Credor &cre)
{
    Conexao      con(connectionName);
    QSqlQuery credor(con.getConnection());
    credor.prepare("update credor set cre_nome = :nome where cre_id = :id;");
    credor.bindValue(":id",   cre.getId().toInt());
    credor.bindValue(":nome", cre.getNome());

    Message *m = new Message;
    if(credor.exec())
    {
        m->setStatus(true);
        m->setMessage("Alterado com sucesso.");
    }
    else
    {
        m->setStatus(false);
        m->setMessage("Ocorreu um erro.<br><br>" + credor.lastError().text());
    }
    return m;
}
