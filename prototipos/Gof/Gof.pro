#-------------------------------------------------
#
# Project created by QtCreator 2015-07-20T11:14:10
#
#-------------------------------------------------

QT       += core network xml sql

QT       += gui

TARGET = Gof
CONFIG   += console
CONFIG   -= app_bundle

TEMPLATE = app


SOURCES += main.cpp \         
    dao/credordao.cpp \
    dao/devedordao.cpp \
    dao/usuariodao.cpp \
    filter/loginfilter.cpp \
    servlets/cadastrarcredorservlet.cpp \
    servlets/cadastrardevedorservlet.cpp \
    servlets/credoresservlet.cpp \
    servlets/deletarcredorservlet.cpp \
    servlets/deletardevedorservlet.cpp \
    servlets/devedoresservlet.cpp \
    servlets/entradasservlet.cpp \
    servlets/homeservlet.cpp \
    servlets/logarservlet.cpp \
    servlets/loginservlet.cpp \
    servlets/saidasservlet.cpp \
    servlets/salvarcredorservlet.cpp \
    servlets/salvardevedorservlet.cpp \
    entidades/credor.cpp \
    entidades/devedor.cpp \
    entidades/message.cpp \
    entidades/usuario.cpp \
    db/conexao.cpp \
    ../CPPWebFramework/cwf/configuration.cpp \
    ../CPPWebFramework/cwf/cppwebapplication.cpp \
    ../CPPWebFramework/cwf/cppwebserver.cpp \
    ../CPPWebFramework/cwf/cstlcompiler.cpp \
    ../CPPWebFramework/cwf/filemanager.cpp \
    ../CPPWebFramework/cwf/filter.cpp \
    ../CPPWebFramework/cwf/filterchain.cpp \    
    ../CPPWebFramework/cwf/httpcookie.cpp \
    ../CPPWebFramework/cwf/httpparser.cpp \
    ../CPPWebFramework/cwf/httpreadrequest.cpp \
    ../CPPWebFramework/cwf/httpservlet.cpp \
    ../CPPWebFramework/cwf/httpservletrequest.cpp \
    ../CPPWebFramework/cwf/httpservletresponse.cpp \
    ../CPPWebFramework/cwf/httpsession.cpp \
    ../CPPWebFramework/cwf/properties.cpp \
    ../CPPWebFramework/cwf/qlistobject.cpp \
    ../CPPWebFramework/cwf/requestdispatcher.cpp \
    ../CPPWebFramework/cwf/sessionidgenerator.cpp \
    ../CPPWebFramework/cwf/cppwebservlet.cpp \
    ../CPPWebFramework/cwf/cstlcompilerattributes.cpp \
    ../CPPWebFramework/cwf/cstlcompilerfor.cpp \
    ../CPPWebFramework/cwf/cstlcompilerif.cpp \
    ../CPPWebFramework/cwf/cstlcompilerobject.cpp \
    ../CPPWebFramework/cwf/forattributes.cpp \
    ../CPPWebFramework/cwf/ifattributes.cpp \
    ../CPPWebFramework/cwf/metaclassparser.cpp

QMAKE_CXXFLAGS += -std=c++11

HEADERS += \  
    dao/credordao.h \
    dao/devedordao.h \
    dao/usuariodao.h \
    filter/loginfilter.h \
    servlets/cadastrarcredorservlet.h \
    servlets/cadastrardevedorservlet.h \
    servlets/credoresservlet.h \
    servlets/deletarcredorservlet.h \
    servlets/deletardevedorservlet.h \
    servlets/devedoresservlet.h \
    servlets/entradasservlet.h \
    servlets/homeservlet.h \
    servlets/logarservlet.h \
    servlets/loginservlet.h \
    servlets/saidasservlet.h \
    servlets/salvarcredorservlet.h \
    servlets/salvardevedorservlet.h \
    entidades/credor.h \
    entidades/devedor.h \
    entidades/message.h \
    entidades/usuario.h \
    db/conexao.h \
    ../CPPWebFramework/cwf/configuration.h \
    ../CPPWebFramework/cwf/cppwebapplication.h \
    ../CPPWebFramework/cwf/cppwebserver.h \
    ../CPPWebFramework/cwf/cstlcompiler.h \
    ../CPPWebFramework/cwf/filemanager.h \
    ../CPPWebFramework/cwf/filter.h \
    ../CPPWebFramework/cwf/filterchain.h \
    ../CPPWebFramework/cwf/httpcookie.h \
    ../CPPWebFramework/cwf/httpparser.h \
    ../CPPWebFramework/cwf/httpreadrequest.h \
    ../CPPWebFramework/cwf/httprequestmethod.h \
    ../CPPWebFramework/cwf/httpservlet.h \
    ../CPPWebFramework/cwf/httpservletrequest.h \
    ../CPPWebFramework/cwf/httpservletresponse.h \
    ../CPPWebFramework/cwf/httpsession.h \
    ../CPPWebFramework/cwf/properties.h \
    ../CPPWebFramework/cwf/qlistobject.h \
    ../CPPWebFramework/cwf/qmapthreadsafety.h \
    ../CPPWebFramework/cwf/requestdispatcher.h \
    ../CPPWebFramework/cwf/sessionidgenerator.h \
    ../CPPWebFramework/cwf/cppwebservlet.h \
    ../CPPWebFramework/cwf/cstlcompilerattributes.h \
    ../CPPWebFramework/cwf/cstlcompilerfor.h \
    ../CPPWebFramework/cwf/cstlcompilerif.h \
    ../CPPWebFramework/cwf/cstlcompilerobject.h \
    ../CPPWebFramework/cwf/forattributes.h \
    ../CPPWebFramework/cwf/ifattributes.h \
    ../CPPWebFramework/cwf/metaclassparser.h


INCLUDEPATH += ../CPPWebFramework/

OTHER_FILES += \
    server/config/CPPWeb.ini \
    server/config/log/CPPWebServer.log

DISTFILES += \
    server/pages/cadastrarcredores.xhtml \
    server/pages/cadastrardevedores.xhtml \
    server/pages/credores.xhtml \
    server/pages/deletarcredores.xhtml \
    server/pages/deletardevedores.xhtml \
    server/pages/devedores.xhtml \
    server/pages/entradas.xhtml \
    server/pages/expired.xhtml \
    server/pages/home.xhtml \
    server/pages/login.xhtml \
    server/pages/saidas.xhtml \
    server/pages/salvarcredores.xhtml \
    server/pages/salvardevedores.xhtml \
    server/pages/teste.xhtml
