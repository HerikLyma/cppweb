#ifndef LOGARSERVLET_H
#define LOGARSERVLET_H

#include "cwf/httpservlet.h"

class LogarServlet : public CWF::HttpServlet
{
public:
    void doPost(CWF::HttpServletRequest &request, CWF::HttpServletResponse &response);
};

#endif // LOGARSERVLET_H
