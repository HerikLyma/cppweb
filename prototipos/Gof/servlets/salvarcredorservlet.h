#ifndef SALVARCREDORSERVLET_H
#define SALVARCREDORSERVLET_H

#include "cwf/httpservlet.h"

class SalvarCredorServlet : public CWF::HttpServlet
{
public:
    ~SalvarCredorServlet();

    void doPost(CWF::HttpServletRequest &request, CWF::HttpServletResponse &response);
};

#endif // SALVARCREDORSERVLET_H
