#ifndef SAIDASSERVLET_H
#define SAIDASSERVLET_H

#include "cwf/httpservlet.h"

class SaidasServlet : public CWF::HttpServlet
{
public:
    ~SaidasServlet();

    void doGet(CWF::HttpServletRequest &request, CWF::HttpServletResponse &response);
};

#endif // SAIDASSERVLET_H
