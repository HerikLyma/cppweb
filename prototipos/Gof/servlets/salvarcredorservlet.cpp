#include "salvarcredorservlet.h"
#include <dao/credordao.h>

SalvarCredorServlet::~SalvarCredorServlet()
{

}

void SalvarCredorServlet::doPost(CWF::HttpServletRequest &request, CWF::HttpServletResponse &response)
{
    CredorDao dao(request.getSession().getId());
    int idCredor = request.getParameter("idCredor").toInt();
    Message *m = nullptr;
    Credor credor;

    credor.setId(request.getParameter("idCredor"));
    credor.setNome(request.getParameter("nome"));
    credor.setUsuId(request.getParameter("idUsuario"));

    if(idCredor != 0)
        m = dao.atualizar(credor);
    else
        m = dao.inserir(credor);

    request.addAttribute("mensagem", m);
    request.getRequestDispatcher("/pages/salvarcredores").forward(request, response);
    delete m;
}
