#include "devedoresservlet.h"


DevedoresServlet::~DevedoresServlet()
{

}

void DevedoresServlet::doGet(CWF::HttpServletRequest &request, CWF::HttpServletResponse &response)
{
    QObject *o = request.getSession().getAttribute("usuario");
    CWF::QListObject *list = nullptr;

    if(o != nullptr)
    {
        Usuario *usu = (Usuario*)o;
        QString id = request.getSession().getId();
        DevedorDao dao(id);
        list = dao.getAll(usu->getId());
        request.addAttribute("devedordao", list);
    }

    request.getRequestDispatcher("/pages/devedores").forward(request, response);
    if(list)
        delete list;
}
