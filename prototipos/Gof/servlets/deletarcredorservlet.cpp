#include "deletarcredorservlet.h"


DeletarCredorServlet::~DeletarCredorServlet()
{

}

void DeletarCredorServlet::doGet(CWF::HttpServletRequest &request, CWF::HttpServletResponse &response)
{
    CredorDao dao(request.getSession().getId());
    Message *m = dao.deletar(request.getParameter("id").toInt());
    request.addAttribute("mensagem", m);
    request.getRequestDispatcher("/pages/deletarcredores").forward(request, response);
    delete m;
}
