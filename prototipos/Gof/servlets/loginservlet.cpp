#include "loginservlet.h"
#include "cwf/requestdispatcher.h"
#include "cwf/httpservletresponse.h"

LoginServlet::~LoginServlet()
{

}

void LoginServlet::doGet(CWF::HttpServletRequest &request, CWF::HttpServletResponse &response)
{
    request.getRequestDispatcher("/pages/login").forward(request, response);
}
