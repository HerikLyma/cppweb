#include "cadastrarcredorservlet.h"
#include <entidades/usuario.h>
#include <dao/credordao.h>

CadastrarCredorServlet::~CadastrarCredorServlet()
{

}

void CadastrarCredorServlet::doGet(CWF::HttpServletRequest &request, CWF::HttpServletResponse &response)
{
    QObject *o = request.getSession().getAttribute("usuario");

    if(o != nullptr)
    {
        request.addAttribute("usuario", o);
    }

    Credor *c = nullptr;
    if(!request.getParameter("id").isEmpty())
    {
        CredorDao credorDao(request.getSession().getId());
        c = credorDao.getById(request.getParameter("id"));
    }
    else
        c = new Credor;

    request.addAttribute("credor", c);
    request.getRequestDispatcher("/pages/cadastrarcredores").forward(request, response);
    delete c;
}
