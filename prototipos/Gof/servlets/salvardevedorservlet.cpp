#include "salvardevedorservlet.h"
#include <dao/devedordao.h>

SalvarDevedorServlet::~SalvarDevedorServlet()
{

}

void SalvarDevedorServlet::doPost(CWF::HttpServletRequest &request, CWF::HttpServletResponse &response)
{
    DevedorDao dao(request.getSession().getId());
    int idDevedor = request.getParameter("idDevedor").toInt();
    Message *m;

    Devedor devedor;

    devedor.setId(request.getParameter("idDevedor"));
    devedor.setNome(request.getParameter("nome"));
    devedor.setUsuId(request.getParameter("idUsuario"));

    if(idDevedor != 0)
        m = dao.atualizar(devedor);
    else
        m = dao.inserir(devedor);

    request.addAttribute("mensagem", m);
    request.getRequestDispatcher("/pages/salvardevedores").forward(request, response);
    delete m;
}

