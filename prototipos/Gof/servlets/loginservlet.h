#ifndef LOGINSERVLET_H
#define LOGINSERVLET_H


#include "cwf/httpservlet.h"

class LoginServlet  : public CWF::HttpServlet
{
public:
    virtual ~LoginServlet();

    void doGet(CWF::HttpServletRequest &request, CWF::HttpServletResponse &response);
};

#endif // LOGINSERVLET_H
