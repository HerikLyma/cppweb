#ifndef DEVEDORESSERVLET_H
#define DEVEDORESSERVLET_H

#include "cwf/httpservlet.h"
#include <entidades/usuario.h>
#include <dao/devedordao.h>
#include "cwf/qlistobject.h"

class DevedoresServlet : public CWF::HttpServlet
{
public:
    ~DevedoresServlet();

    void doGet(CWF::HttpServletRequest &request, CWF::HttpServletResponse &response);
};

#endif // DEVEDORESSERVLET_H
