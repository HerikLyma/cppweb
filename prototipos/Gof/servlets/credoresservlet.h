#ifndef CREDORESSERVLET_H
#define CREDORESSERVLET_H

#include "cwf/httpservlet.h"
#include <entidades/usuario.h>
#include <dao/credordao.h>
#include "cwf/qlistobject.h"


class CredoresServlet : public CWF::HttpServlet
{
public:
    ~CredoresServlet();

    void doGet(CWF::HttpServletRequest &request, CWF::HttpServletResponse &response);
};

#endif // CREDORESSERVLET_H
