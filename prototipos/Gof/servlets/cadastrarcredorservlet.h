#ifndef CADASTRARCREDORSERVLET_H
#define CADASTRARCREDORSERVLET_H

#include "cwf/httpservlet.h"

class CadastrarCredorServlet : public CWF::HttpServlet
{
public:    
    ~CadastrarCredorServlet();

    void doGet(CWF::HttpServletRequest &request, CWF::HttpServletResponse &response);
};

#endif // CADASTRARCREDORSERVLET_H
