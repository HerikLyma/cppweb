#ifndef HOMESERVLET_H
#define HOMESERVLET_H

#include "cwf/httpservlet.h"

class HomeServlet : public CWF::HttpServlet
{
public:    
    virtual ~HomeServlet();

    void doGet(CWF::HttpServletRequest &request, CWF::HttpServletResponse &response);
};

#endif // HOMESERVLET_H
