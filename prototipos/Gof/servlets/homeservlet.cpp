#include "homeservlet.h"
#include "cwf/requestdispatcher.h"
#include "cwf/httpservletresponse.h"
#include <entidades/usuario.h>


HomeServlet::~HomeServlet()
{
}


void HomeServlet::doGet(CWF::HttpServletRequest &request, CWF::HttpServletResponse &response)
{
    request.getRequestDispatcher("/pages/home").forward(request, response);
}
