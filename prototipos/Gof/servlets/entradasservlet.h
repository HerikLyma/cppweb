#ifndef ENTRADASSERVLET_H
#define ENTRADASSERVLET_H

#include "cwf/httpservlet.h"

class EntradasServlet : public CWF::HttpServlet
{
public:
    ~EntradasServlet();

    void doGet(CWF::HttpServletRequest &request, CWF::HttpServletResponse &response);
};

#endif // ENTRADASSERVLET_H
