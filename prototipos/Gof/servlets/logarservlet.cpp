#include "logarservlet.h"
#include <entidades/usuario.h>
#include "cwf/requestdispatcher.h"
#include <dao/usuariodao.h>

void LogarServlet::doPost(CWF::HttpServletRequest &request, CWF::HttpServletResponse &response)
{
    QString login = request.getParameter("login");
    QString senha = request.getParameter("senha");

    Usuario *usuario = UsuarioDao(request.getSession().getId()).logar(login, senha);

    if(usuario != nullptr)
    {                
        request.getSession().validate();
        request.getSession().addAttribute("usuario", usuario);
        request.getRequestDispatcher("/pages/home").forward(request, response);
    }
}
