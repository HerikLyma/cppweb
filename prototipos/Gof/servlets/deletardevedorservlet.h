#ifndef DELETARDEVEDORSERVLET_H
#define DELETARDEVEDORSERVLET_H

#include "cwf/httpservlet.h"

class DeletarDevedorServlet : public CWF::HttpServlet
{
public:
    ~DeletarDevedorServlet();

    void doGet(CWF::HttpServletRequest &request, CWF::HttpServletResponse &response);
};

#endif // DELETARDEVEDORSERVLET_H
