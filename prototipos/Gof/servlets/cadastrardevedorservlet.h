#ifndef CADASTRARDEVEDORSERVLET_H
#define CADASTRARDEVEDORSERVLET_H

#include "cwf/httpservlet.h"

class CadastrarDevedorServlet : public CWF::HttpServlet
{
public:
    ~CadastrarDevedorServlet();

    void doGet(CWF::HttpServletRequest &request, CWF::HttpServletResponse &response);

};

#endif // CADASTRARDEVEDORSERVLET_H
