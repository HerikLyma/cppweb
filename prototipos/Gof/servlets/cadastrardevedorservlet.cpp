#include "cadastrardevedorservlet.h"
#include <dao/devedordao.h>
#include <entidades/usuario.h>

CadastrarDevedorServlet::~CadastrarDevedorServlet()
{

}

void CadastrarDevedorServlet::doGet(CWF::HttpServletRequest &request, CWF::HttpServletResponse &response)
{
    QObject *o = request.getSession().getAttribute("usuario");

    if(o != nullptr)
    {
        request.addAttribute("usuario", o);
    }

    Devedor *c = nullptr;
    if(!request.getParameter("id").isEmpty())
    {
        DevedorDao dao(request.getSession().getId());
        c = dao.getById(request.getParameter("id"));
    }
    else
        c = new Devedor;

    request.addAttribute("devedor", c);
    request.getRequestDispatcher("/pages/cadastrardevedores").forward(request, response);

    delete c;
}

