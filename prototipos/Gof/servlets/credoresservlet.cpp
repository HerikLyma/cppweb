#include "credoresservlet.h"


CredoresServlet::~CredoresServlet()
{

}

void CredoresServlet::doGet(CWF::HttpServletRequest &request, CWF::HttpServletResponse &response)
{
    QObject     *o = request.getSession().getAttribute("usuario");
    CWF::QListObject *list = nullptr;

    if(o != nullptr)
    {
        Usuario *usu = (Usuario*)o;
        QString id = request.getSession().getId();
        CredorDao dao(id);
        list = dao.getAll(usu->getId());
        request.addAttribute("credordao", list);
    }

    request.getRequestDispatcher("/pages/credores").forward(request, response);
    if(list)
        delete list;
}
