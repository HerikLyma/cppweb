#include "deletardevedorservlet.h"
#include <dao/devedordao.h>

DeletarDevedorServlet::~DeletarDevedorServlet()
{

}

void DeletarDevedorServlet::doGet(CWF::HttpServletRequest &request, CWF::HttpServletResponse &response)
{
    DevedorDao dao(request.getSession().getId());
    Message *m = dao.deletar(request.getParameter("id").toInt());
    request.addAttribute("mensagem", m);
    request.getRequestDispatcher("/pages/deletardevedores").forward(request, response);
    delete m;
}
