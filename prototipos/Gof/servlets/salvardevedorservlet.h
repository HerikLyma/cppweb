#ifndef SALVARDEVEDORSERVLET_H
#define SALVARDEVEDORSERVLET_H

#include "cwf/httpservlet.h"

class SalvarDevedorServlet : public CWF::HttpServlet
{
public:
    ~SalvarDevedorServlet();

    void doPost(CWF::HttpServletRequest &request, CWF::HttpServletResponse &response);
};

#endif // SALVARDEVEDORSERVLET_H
