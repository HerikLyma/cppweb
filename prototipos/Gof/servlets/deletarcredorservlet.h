#ifndef DELETARCREDORSERVLET_H
#define DELETARCREDORSERVLET_H

#include "cwf/httpservlet.h"
#include <dao/credordao.h>

class DeletarCredorServlet : public CWF::HttpServlet
{
public:
    ~DeletarCredorServlet();

    void doGet(CWF::HttpServletRequest &request, CWF::HttpServletResponse &response);
};

#endif // DELETARCREDORSERVLET_H
