#include "frmmain.h"
#include "ui_frmmain.h"
#include "cwf/cstlcompiler.h"
#include "cwf/cstlcompilerobject.h"

FrmMain::FrmMain(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::FrmMain)
{
    ui->setupUi(this);
}

FrmMain::~FrmMain()
{
    delete ui;
}

void FrmMain::on_pshBtnCompile_clicked()
{
    CWF::CSTLCompilerObject var;
    var.setValue("http://www.cpplovers.com");

    QMap<QString, QObject *> obj;
    obj.insert("var", &var);

    CWF::CSTLCompiler compiler(ui->plnTxtEdtInput->toPlainText().toStdString().data(), obj, false);
    ui->plnTxtEdtOutput->setPlainText(compiler.output().data());

    /*
    QXmlStreamReader reader;
    reader.addData(ui->plnTxtEdtInput->toPlainText()); // data is a QByteArray obtained from the API call
    while(reader.readNextStartElement())
    {
        //qDebug() << reader.name();
        if (reader.name() == "for")
        {
            while(reader.readNextStartElement())
            {
                qDebug() << reader.name() << reader.readElementText();
            }
        }
    }
    */
}
