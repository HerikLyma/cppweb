#ifndef MULTIFILESERVLET_H
#define MULTIFILESERVLET_H

#include "cwf/httpservlet.h"

class MultiFileServlet : public CWF::HttpServlet
{
public:
    void doPost(CWF::HttpServletRequest &req, CWF::HttpServletResponse &resp);

    void doGet(CWF::HttpServletRequest &req, CWF::HttpServletResponse &resp);
};

#endif // MULTIFILESERVLET_H
