#include "clientservlet.h"
#include <entidades/cliente.h>
#include "cwf/httpservletrequest.h"
#include "cwf/httpservletresponse.h"
#include "cwf/requestdispatcher.h"

ClientServlet::~ClientServlet()
{
}

void ClientServlet::doGet(CWF::HttpServletRequest &req, CWF::HttpServletResponse &resp)
{   
    Cliente cliente1;
    Cliente cliente2;
    Cliente cliente3;

    cliente1.setNome("Herik");
    cliente2.setNome("Camila");
    cliente3.setNome("Marcelo");

    req.addAttribute("cliente1", &cliente1);
    req.addAttribute("cliente2", &cliente2);
    req.addAttribute("cliente3", &cliente3);

    req.getRequestDispatcher("/pages/cliente").forward(req, resp);
}
