#include "forservlet.h"
#include "cwf/httpservletrequest.h"
#include "entidades/cliente.h"

void ForServlet::doGet(CWF::HttpServletRequest &req, CWF::HttpServletResponse &resp)
{
    Cliente cliente;
    cliente.setNome("Herik");
    req.addAttribute("cliente", &cliente);

    req.getRequestDispatcher("/pages/for").forward(req, resp);
}
