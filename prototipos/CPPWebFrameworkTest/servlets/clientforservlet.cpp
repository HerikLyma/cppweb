#include "clientforservlet.h"
#include <dao/clientedao.h>
#include "cwf/httpservletrequest.h"
#include "cwf/httpservletresponse.h"
#include "cwf/requestdispatcher.h"


ClientForServlet::~ClientForServlet()
{
}

void ClientForServlet::doGet(CWF::HttpServletRequest &req, CWF::HttpServletResponse &resp)
{
    ClienteDao cliDao;
    CWF::QListObject *list = cliDao.getAll();
    req.addAttribute("clientedao", list);
    req.getRequestDispatcher("/pages/clientefor").forward(req, resp);
    delete list;
}
