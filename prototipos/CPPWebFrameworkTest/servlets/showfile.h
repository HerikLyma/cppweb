#ifndef SHOWFILE_H
#define SHOWFILE_H

#include "cwf/httpservlet.h"

class showfile : public CWF::HttpServlet
{
public:
    void doPost(CWF::HttpServletRequest &req, CWF::HttpServletResponse &resp);

    void doGet(CWF::HttpServletRequest &req, CWF::HttpServletResponse &resp);
};

#endif // SHOWFILE_H
