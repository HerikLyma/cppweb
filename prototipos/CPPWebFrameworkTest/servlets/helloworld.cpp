#include "helloworld.h"
#include <cwf/httpservletresponse.h>

void HelloWorld::doGet(CWF::HttpServletRequest &req, CWF::HttpServletResponse &resp)
{
    Q_UNUSED(req)
    resp.write("<html><body>Hello World!!!</body></html>");
}
