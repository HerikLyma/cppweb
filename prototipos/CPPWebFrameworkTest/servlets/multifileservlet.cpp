#include "multifileservlet.h"
#include <QFile>
#include "cwf/httpservletrequest.h"
#include "cwf/httpservletresponse.h"

void MultiFileServlet::doPost(CWF::HttpServletRequest &req, CWF::HttpServletResponse &resp)
{
    QByteArray html(std::move("<html><head><title>Sucesso</title></head><body><center><font color=\"red\" size = \"6\">"));
    auto files(std::move(req.getUploadedFiles()));
    if(files.size())
    {
        for(auto it = files.begin(); it != files.end(); ++it)
        {
            QFile file("/home/herik/" + it.key());
            file.open(QIODevice::WriteOnly);
            file.write(it.value());
            file.close();
        }
        html += QByteArray::number(files.size()) + " Arquivos salvos.\n";
    }
    else
    {
        html.replace("Sucesso", "Falha");
        html += "Nenhum arquivo salvo.\n";
    }
    html += "</font></center></body></html>";
    resp.write(html);
}

void MultiFileServlet::doGet(CWF::HttpServletRequest &req, CWF::HttpServletResponse &resp)
{
    req.getRequestDispatcher("/pages/multifile").forward(req, resp);
}
