#include "videoservlet.h"
#include <QFile>
#include <QDir>
#include <QMutex>


void VideoServlet::doPost(CWF::HttpServletRequest &req, CWF::HttpServletResponse &resp)
{
    auto files(std::move(req.getUploadedFiles()));
    if(files.size())
    {
        for(auto it = files.begin(); it != files.end(); ++it)
        {
            const QString sessionId = req.getSession().getId();
            QString path(std::move("/home/herik/cppweb/prototipos/CPPWebFrameworkTest/server/pages/"));
            if(!QDir(path + sessionId).exists())
                QDir().mkdir(path + sessionId);

            QString fileName = path + sessionId + "/" + it.key();
            QFile file(fileName);
            file.open(QIODevice::WriteOnly);
            file.write(it.value());
            file.close();


            resp.sendRedirect(("/pages/" + sessionId + "/" + it.key()).toLatin1());

            break;
        }
    }
    else
    {
        resp.write("<html><head><title>Falha</title></head><body><center><font color=\"red\" size = \"6\">Nenhum arquivo salvo.\n</font></center></body></html>");
    }
}

void VideoServlet::doGet(CWF::HttpServletRequest &req, CWF::HttpServletResponse &resp)
{
    req.getRequestDispatcher("/pages/video").forward(req, resp);
}
