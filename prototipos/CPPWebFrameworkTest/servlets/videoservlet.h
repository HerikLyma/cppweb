#ifndef VIDEOSERVLET_H
#define VIDEOSERVLET_H

#include <QFile>
#include "cwf/httpservlet.h"
#include "cwf/httpservletrequest.h"
#include "cwf/httpservletresponse.h"
#include "cwf/filemanager.h"



class VideoServlet : public CWF::HttpServlet
{
public:
    void doPost(CWF::HttpServletRequest &req, CWF::HttpServletResponse &resp);

    void doGet(CWF::HttpServletRequest &req, CWF::HttpServletResponse &resp);
};

#endif // VIDEOSERVLET_H
