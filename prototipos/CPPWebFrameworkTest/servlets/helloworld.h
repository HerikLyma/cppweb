#ifndef HELLOWORLD_H
#define HELLOWORLD_H

#include <cwf/httpservlet.h>

class HelloWorld : public CWF::HttpServlet
{
public:
    void doGet(CWF::HttpServletRequest &req, CWF::HttpServletResponse &resp);
};

#endif // HELLOWORLD_H
