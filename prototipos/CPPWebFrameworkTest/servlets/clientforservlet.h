#ifndef CLIENTFORSERVLET_H
#define CLIENTFORSERVLET_H

#include "cwf/httpservlet.h"

class ClientForServlet : public CWF::HttpServlet
{
public:
    virtual ~ClientForServlet();

    void doGet(CWF::HttpServletRequest &req, CWF::HttpServletResponse &resp);
};

#endif // CLIENTFORSERVLET_H
