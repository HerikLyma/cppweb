#ifndef FORSERVLET_H
#define FORSERVLET_H

#include "cwf/httpservlet.h"

class ForServlet : public CWF::HttpServlet
{
public:    
    void doGet(CWF::HttpServletRequest &req, CWF::HttpServletResponse &resp);
};

#endif // FORSERVLET_H
