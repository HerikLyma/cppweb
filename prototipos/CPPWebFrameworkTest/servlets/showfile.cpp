#include "showfile.h"
#include "cwf/httpservletrequest.h"
#include "cwf/httpservletresponse.h"
#include "cwf/filemanager.h"

void showfile::doPost(CWF::HttpServletRequest &req, CWF::HttpServletResponse &resp)
{
    auto files(std::move(req.getUploadedFiles()));
    if(files.size())
    {
        for(auto it = files.begin(); it != files.end(); ++it)
        {
            QString fileName = it.key();
            QString extention = CWF::FileManager().fileExtention(fileName);
            extention = extention.toLower();
            if(extention == "png" || extention == "gif" || extention == "bmp")
                resp.addHeader("Content-Type", ("image/" + extention.toLatin1()));
            else if(extention == "jpe" || extention == "jpg")
                resp.addHeader("Content-Type", "image/jpeg");

            resp.addHeader("content-disposition", ("inline; filename=" + fileName.toLatin1()));
            resp.write(it.value());
            break;
        }
    }
    else
    {
        resp.write("<html><head><title>Falha</title></head><body><center><font color=\"red\" size = \"6\">Nenhum arquivo salvo.\n</font></center></body></html>");
    }
}

void showfile::doGet(CWF::HttpServletRequest &req, CWF::HttpServletResponse &resp)
{
    req.getRequestDispatcher("/pages/showfile").forward(req, resp);
}
