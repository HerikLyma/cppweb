#ifndef CLIENTSERVLET_H
#define CLIENTSERVLET_H

#include "cwf/httpservlet.h"

class ClientServlet : public CWF::HttpServlet
{
public:
    virtual ~ClientServlet();

    void doGet(CWF::HttpServletRequest &req, CWF::HttpServletResponse &resp);
};

#endif // CLIENTSERVLET_H
