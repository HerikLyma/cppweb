#include "cliente.h"

Cliente::Cliente(QObject *parent) : QObject(parent){}

QString Cliente::getNome()
{
    return nome;
}

QString Cliente::getEndereco()
{
    return endereco;
}

void Cliente::setNome(QString nome)
{
    this->nome = nome;
}

void Cliente::setEndereco(QString endereco)
{
    this->endereco = endereco;
}
