#ifndef CLIENTE_H
#define CLIENTE_H

#include <QObject>

class Cliente : public QObject
{
    Q_OBJECT    
public:
    explicit Cliente(QObject *parent = 0);  
private:
    QString nome;
    QString endereco;
public slots:
    QString getNome();
    QString getEndereco();
    void    setNome(QString nome);
    void    setEndereco(QString endereco);
};

#endif // CLIENTE_H
