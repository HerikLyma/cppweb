#include "cwf/cppwebapplication.h"
#include "servlets/helloworld.h"
#include "servlets/showfile.h"
#include "servlets/clientforservlet.h"
#include "servlets/clientservlet.h"
#include "servlets/multifileservlet.h"
#include "servlets/videoservlet.h"
#include "servlets/forservlet.h"

int main(int argc, char *argv[])
{
    CWF::CppWebApplication server(argc, argv, CWF::Configuration("/home/herik/cppweb/prototipos/CPPWebFrameworkTest/server/"));

    server.addUrlServlet("/clientfor" , new ClientForServlet);
    server.addUrlServlet("/client*"    , new ClientServlet);
    server.addUrlServlet("/showfile"  , new showfile);
    server.addUrlServlet("/multifile" , new MultiFileServlet);
    server.addUrlServlet("/video"     , new VideoServlet);
    server.addUrlServlet("/for"       , new ForServlet);


    return server.start();
}
