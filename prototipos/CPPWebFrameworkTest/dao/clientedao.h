#ifndef CLIENTEDAO_H
#define CLIENTEDAO_H

#include <entidades/cliente.h>
#include "cwf/qlistobject.h"

class ClienteDao
{
public:
    CWF::QListObject *getAll();
};

#endif // CLIENTEDAO_H
