#include "clientedao.h"

CWF::QListObject *ClienteDao::getAll()
{
    CWF::QListObject *list = new CWF::QListObject;

    Cliente *cliente0 = new Cliente;
    Cliente *cliente1 = new Cliente;
    Cliente *cliente2 = new Cliente;
    Cliente *cliente3 = new Cliente;
    Cliente *cliente4 = new Cliente;
    Cliente *cliente5 = new Cliente;
    Cliente *cliente6 = new Cliente;
    Cliente *cliente7 = new Cliente;
    Cliente *cliente8 = new Cliente;
    Cliente *cliente9 = new Cliente;

    cliente0->setNome("Herik");
    cliente0->setEndereco("Rua Herik");

    cliente1->setNome("Camila");
    cliente1->setEndereco("Rua Camila");

    cliente2->setNome("Gabriel");
    cliente2->setEndereco("Rua Gabriel");

    cliente3->setNome("Marcelo");
    cliente3->setEndereco("Rua Marcelo");

    cliente3->setNome("Jose");
    cliente3->setEndereco("Rua Jose");

    cliente4->setNome("Maira");
    cliente4->setEndereco("Rua Maria");

    cliente5->setNome("Pedro");
    cliente5->setEndereco("Rua Pedro");

    cliente6->setNome("Thiago");
    cliente6->setEndereco("Rua Thiago");

    cliente7->setNome("Juan");
    cliente7->setEndereco("Rua Juan");

    cliente8->setNome("Jose");
    cliente8->setEndereco("Rua Jose");

    cliente9->setNome("Lucio");
    cliente9->setEndereco("Rua Lucio");

    list->add(cliente0);
    list->add(cliente1);
    list->add(cliente2);
    list->add(cliente3);
    list->add(cliente4);
    list->add(cliente5);
    list->add(cliente6);
    list->add(cliente7);
    list->add(cliente8);
    list->add(cliente9);

    return list;
}
