#include <QCoreApplication>
#include <servlets/helloworldservlet.h>
#include "cwf/cppwebapplication.h"

int main(int argc, char *argv[])
{        
    CWF::CppWebApplication server(argc, argv,
                                  CWF::Configuration("/home/herik/cppweb/prototipos/HelloWorld/server"));



    server.addUrlServlet("/hello", new HelloWorldServlet);


    return server.start();
}

/** \example /home/herik/cppweb/prototipos/HelloWorld/servlets/helloworldservlet.cpp
 * This is an example of how to use the Test class.
 * More details about this example.
 */

