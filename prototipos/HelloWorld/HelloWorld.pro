#-------------------------------------------------
#
# Project created by QtCreator 2015-07-20T11:14:10
#
#-------------------------------------------------

QT       += core network xml sql

TARGET = HelloWorld
CONFIG   += console
CONFIG   -= app_bundle

TEMPLATE = app


SOURCES += helloworld.cpp \     
    servlets/helloworldservlet.cpp \
    ../CPPWebFramework/cwf/configuration.cpp \
    ../CPPWebFramework/cwf/cppwebapplication.cpp \
    ../CPPWebFramework/cwf/cppwebserver.cpp \
    ../CPPWebFramework/cwf/cstlcompiler.cpp \
    ../CPPWebFramework/cwf/filemanager.cpp \
    ../CPPWebFramework/cwf/filter.cpp \
    ../CPPWebFramework/cwf/filterchain.cpp \    
    ../CPPWebFramework/cwf/httpcookie.cpp \
    ../CPPWebFramework/cwf/httpparser.cpp \
    ../CPPWebFramework/cwf/httpreadrequest.cpp \
    ../CPPWebFramework/cwf/httpservlet.cpp \
    ../CPPWebFramework/cwf/httpservletrequest.cpp \
    ../CPPWebFramework/cwf/httpservletresponse.cpp \
    ../CPPWebFramework/cwf/httpsession.cpp \
    ../CPPWebFramework/cwf/properties.cpp \
    ../CPPWebFramework/cwf/qlistobject.cpp \
    ../CPPWebFramework/cwf/requestdispatcher.cpp \
    ../CPPWebFramework/cwf/sessionidgenerator.cpp \
    ../CPPWebFramework/cwf/cppwebservlet.cpp \
    ../CPPWebFramework/cwf/cstlcompilerattributes.cpp \
    ../CPPWebFramework/cwf/cstlcompilerfor.cpp \
    ../CPPWebFramework/cwf/cstlcompilerif.cpp \
    ../CPPWebFramework/cwf/cstlcompilerobject.cpp \
    ../CPPWebFramework/cwf/forattributes.cpp \
    ../CPPWebFramework/cwf/ifattributes.cpp \
    ../CPPWebFramework/cwf/metaclassparser.cpp



HEADERS += \    
    servlets/helloworldservlet.h \
    ../CPPWebFramework/cwf/configuration.h \
    ../CPPWebFramework/cwf/cppwebapplication.h \
    ../CPPWebFramework/cwf/cppwebserver.h \
    ../CPPWebFramework/cwf/cstlcompiler.h \
    ../CPPWebFramework/cwf/filemanager.h \
    ../CPPWebFramework/cwf/filter.h \
    ../CPPWebFramework/cwf/filterchain.h \    
    ../CPPWebFramework/cwf/httpcookie.h \
    ../CPPWebFramework/cwf/httpparser.h \
    ../CPPWebFramework/cwf/httpreadrequest.h \
    ../CPPWebFramework/cwf/httprequestmethod.h \
    ../CPPWebFramework/cwf/httpservlet.h \
    ../CPPWebFramework/cwf/httpservletrequest.h \
    ../CPPWebFramework/cwf/httpservletresponse.h \
    ../CPPWebFramework/cwf/httpsession.h \
    ../CPPWebFramework/cwf/properties.h \
    ../CPPWebFramework/cwf/qlistobject.h \
    ../CPPWebFramework/cwf/qmapthreadsafety.h \
    ../CPPWebFramework/cwf/requestdispatcher.h \
    ../CPPWebFramework/cwf/sessionidgenerator.h \
    ../CPPWebFramework/cwf/cppwebservlet.h \
    ../CPPWebFramework/cwf/cstlcompilerattributes.h \
    ../CPPWebFramework/cwf/cstlcompilerfor.h \
    ../CPPWebFramework/cwf/cstlcompilerif.h \
    ../CPPWebFramework/cwf/cstlcompilerobject.h \
    ../CPPWebFramework/cwf/forattributes.h \
    ../CPPWebFramework/cwf/ifattributes.h \
    ../CPPWebFramework/cwf/metaclassparser.h


INCLUDEPATH += ../CPPWebFramework/

QMAKE_CXXFLAGS += -std=c++11

OTHER_FILES += \
    server/config/CPPWeb.ini \
    server/log/CPPWebServer.log \
    server/config/log/CPPWebServer.log
